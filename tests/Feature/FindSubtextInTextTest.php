<?php

namespace Tests\Feature;

use App\Model\Text;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FindSubtextInTextTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $findSubtext = new Text();
        $this->assertEquals('1, 36', $findSubtext->processSearch('Google is a search engine service, google is also an engine for a lot of other services and tools', 'Google'));
        $this->assertEquals('1, 36', $findSubtext->processSearch('Google is a search engine service, google is also an engine for a lot of other services and tools', 'google'));
        $this->assertEquals('2, 37, 94', $findSubtext->processSearch('Google is a search engine service, google is also an engine for a lot of other services and tools', 'oo'));
        $this->assertEquals('2, 37, 94', $findSubtext->processSearch('Google is a search engine service, google is also an engine for a lot of other services and tools', 'Oo'));
        $this->assertEquals('11, 152, 436, 51, 65, 89', $findSubtext->processSearch('Google is a search engine service, google is also an engine for a lot of other services and tools', 'a'));
        $this->assertEquals('no matches', $findSubtext->processSearch('Google is a search engine service, google is also an engine for a lot of other services and tools', 'X'));
    }
}
