<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}"/>
</head>
<body>
<div class="col-md-12">
    <div class="form-search col-lg-6 col-lg-offset-3">
        <h2 class="title-form">Form search sub text</h2>
        <div class="col-lg-12">
            <form id="form-search" action="{{ route('search-position-text') }}" method="post" role="form">
                {!! csrf_field() !!}
                <div class="row">
                    <div class="col-lg-3">
                        <label class="title_text">Text</label>
                    </div>
                    <div class="col-lg-9">
                        <textarea name="text_input" id="text_input" class="form-control" required>
                            {{ old('text_input') }}
                        </textarea>
                    </div>

                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <label class="title_sub_text">Subtext</label>
                    </div>
                    <div class="col-lg-9">
                        <input type="text" name="sub_text_input" id="sub_text_input" class="form-control" value="{{ old('sub_text_input') }}">
                    </div>

                </div>
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3">
                        <button class="btn-primary" id="s_search">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Result Search</h4>
                </div>

                <div class="modal-body">

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="{{URL::asset('js/search.js')}}"></script>
</html>
