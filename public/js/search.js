/**
 * Created by camvt on 03/02/2018.
 */
$(document).ready(function () {
    $('#s_search').click(function () {
        $('.modal-body').empty();
        var formSearch = $('#form-search');
        formSearch.unbind('submit').submit(function (e) {
            e.preventDefault();
            var dataInput = new FormData(this);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: formSearch.attr('action'),
                type: formSearch.attr('method'),
                cache: false,
                contentType: false,
                processData: false,
                data: dataInput,
                success: function (data) {
                    if(data.success){
                        $('.modal-body').append('<b>' + 'Position appear: ' + '</b>' + data.position);
                        $('#myModal').modal({
                            show: 'true'
                        });
                    }
                }
            });
        });
    });
});