<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FindSubTextController@formSearchPositionText')->name('form-search-position-text');
Route::post('/', 'FindSubTextController@searchPositionText')->name('search-position-text');
