<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Text extends Model
{
    /**
     * Process search
     *
     * @param $mainText
     * @param $subText
     * @return string
     */
    public function processSearch($mainText, $subText)
    {
        $result = '';
        $mainTextLower = $this->toLower(trim($mainText));
        $subTextLower = $this->toLower(trim($subText));

        if (strlen($mainTextLower) < strlen($subTextLower) || strlen($subTextLower) == 0 || strlen($mainTextLower) == 0) {
            return 'no matches';
        }
        for ($i = 0; $i < strlen($mainTextLower); $i++) {
            if ($mainTextLower[$i] == $subTextLower[0] && ($i + strlen($subTextLower) - 1) < strlen($mainTextLower)) {
                $checkSuccess = true;
                $location = $i;
                for ($j = 0; $j < strlen($subTextLower); $j++) {

                    if ($subTextLower[$j] != $mainTextLower[$i + $j]) {
                        $checkSuccess = false;
                        $i = $location + 1;
                        continue;
                    }
                }
                if ($checkSuccess) {
                    $result .= strlen($result) == 0 ? ($location + 1) : ', ' . ($location + 1);
                }
            }
        }

        return strlen($result) == 0 ? 'no matches' : $result;
    }

    /**
     * To lower text input
     *
     * @param $textInput
     * @return string
     */
    private function toLower($textInput)
    {
        $result = '';
        for ($i = 0; $i < strlen($textInput); $i++) {
            $asciiCode = $this->utfCharToNumber($textInput[$i]);

            if (65 <= $asciiCode && $asciiCode <= 90) {
                $asciiCode = $asciiCode + (97 - 65);
            }
            $result .= chr($asciiCode);
        }

        return $result;
    }

    /**
     * Change char to number
     *
     * @param $char
     * @return string
     */
    private function utfCharToNumber($char)
    {
        $i = 0;
        $number = '';
        while (isset($char{$i})) {
            $number .= ord($char{$i});
            ++$i;
        }

        return $number;
    }
}
