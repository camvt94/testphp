<?php

namespace App\Http\Controllers;

use App\Model\Text;
use Illuminate\Http\Request;

class FindSubTextController extends Controller
{
    /**
     * @var Text
     */
    private $text;

    /**
     * Constructor
     *
     * FindSubTextController constructor.
     * @param Text $text
     */
    public function __construct(Text $text)
    {
        $this->text = $text;
    }

    /**
     * Get form search
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function formSearchPositionText()
    {
        return view('search.form-search');
    }

    /**
     * Handler search position subtext
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchPositionText(Request $request)
    {
        if ($request->ajax()) {
            $positionSubText = $this->text->processSearch($request->get('text_input'), $request->get('sub_text_input'));
            return response()->json([
                'success' => true,
                'position' => $positionSubText
            ]);
        }
    }
}
